﻿using complaint_site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace complaint_site.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            //Pojedynczy element
            //Tariff tariff = db.Tariffs.FirstOrDefault();
            //return View(tariff);

            return View(db.Tariffs.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Regulations()
        {
            return View();
        }
        public ActionResult Unauthorized()
        {
            return View();
        }
    }
}