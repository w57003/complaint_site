﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using complaint_site.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace complaint_site.Controllers
{
    [YourCustomAuthorize(Roles = "Admin, Worker")]
    public class CustomersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

        [Authorize(Roles = "Admin, Worker")]

        public ActionResult Index()
        {
            var customers = db.Customers.Include(c => c.Tariff);
            return View(customers.ToList());

        }

        [Authorize(Roles = "Admin, Worker")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Worker")]
        public ActionResult LockUnlockAccountChnage(string locked, string unlocked)
        {
            if (locked != null)
            {
                Customer customer = db.Customers.Where(w => w.User.Id == locked).FirstOrDefault();
                DateTime dateLockAccount = new DateTime(2100, 1, 1);
                customer.User.LockoutEndDateUtc = dateLockAccount;
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.Modal = "RUN!";
                return View("Details", customer);
            }
            else if (unlocked != null)
            {
                Customer customer = db.Customers.Where(w => w.User.Id == unlocked).FirstOrDefault();
                customer.User.LockoutEndDateUtc = null;
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.Modal = "RUN!";
                return View("Details", customer);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            ViewBag.IdTariff = new SelectList(db.Tariffs, "IdTariff", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdCustomer,Name,Surname,City,Street,HouseNumber,ZIPCode,CityCorespondence,StreetCorespondence,HouseNumberCorespondence,ZIPCodeCorespondence,Pesel,IdNumber,Phone,IdTariff")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdTariff = new SelectList(db.Tariffs, "IdTariff", "Name", customer.IdTariff);
            return View(customer);
        }

        [Authorize(Roles = "Admin, Worker")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);

            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdTariff = new SelectList(db.Tariffs, "IdTariff", "Name", customer.IdTariff);
            return View(customer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdCustomer,Name,Surname,City,Street,HouseNumber,ZIPCode,CityCorespondence,StreetCorespondence,HouseNumberCorespondence,ZIPCodeCorespondence,Pesel,IdNumber,Phone,IdTariff")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdTariff = new SelectList(db.Tariffs, "IdTariff", "Name", customer.IdTariff);
            return View(customer);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
    public class YourCustomAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (this.AuthorizeCore(filterContext.HttpContext))
            {
                base.OnAuthorization(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectResult("/Home/Unauthorized");
            }
        }
    }
}
