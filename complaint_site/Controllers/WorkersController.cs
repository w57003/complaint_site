﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using complaint_site.Models;
using complaint_site.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace complaint_site.Controllers
{
    [YourCustomAuthorize(Roles = "Admin")]
    public class WorkersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

        public ActionResult Index()
        {
            var data = db.Workers.ToList();
            var returnmodel = data.Select(item => new WorkerViewModel
            {
                IdWorker = item.IdWorker,
                Name = item.Name,
                Surname = item.Surname,
                City = item.City,
                Street = item.Street,
                HouseNumber = item.HouseNumber,
                ZIPCode = item.ZIPCode,
                WorkPhone = item.WorkPhone
            }
            );
            return View(returnmodel);
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Worker worker = db.Workers.Find(id);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(WorkerViewModel worker)
        {
            Worker NewWorker = new Worker
            {
                Name = worker.Name,
                Surname = worker.Surname,
                PrivatePhone = worker.PrivatePhone,
                WorkPhone = worker.WorkPhone,
                City = worker.City,
                Street = worker.Street,
                HouseNumber = worker.HouseNumber,
                ZIPCode = worker.ZIPCode,
                Pesel = worker.Pesel,
                IdNumber = worker.IdNumber
            };

            if (ModelState.IsValid)
            {
                var getUserName = NewWorker.Name[0] + NewWorker.Surname + "@isp.com";
                var getPasswordName = NewWorker.Surname + "." + NewWorker.Pesel[7] + NewWorker.Pesel[8] 
                    + NewWorker.Pesel[9] + NewWorker.Pesel[10];
                getUserName = getUserName.Replace('Ą', 'A').Replace('ą', 'a')
                    .Replace('Ć', 'C').Replace('ć', 'c')
                    .Replace('Ę', 'E').Replace('ę', 'e')
                    .Replace('Ł', 'L').Replace('ł', 'l')
                    .Replace('Ń', 'N').Replace('ń', 'n')
                    .Replace('Ó', 'O').Replace('ó', 'o')
                    .Replace('Ś', 'S').Replace('ś', 's')
                    .Replace('Ź', 'Z').Replace('ź', 'z')
                    .Replace('Ż', 'Z').Replace('ż', 'z');
                getPasswordName = getPasswordName.Replace('Ą', 'A').Replace('ą', 'a')
                    .Replace('Ć', 'C').Replace('ć', 'c')
                    .Replace('Ę', 'E').Replace('ę', 'e')
                    .Replace('Ł', 'L').Replace('ł', 'l')
                    .Replace('Ń', 'N').Replace('ń', 'n')
                    .Replace('Ó', 'O').Replace('ó', 'o')
                    .Replace('Ś', 'S').Replace('ś', 's')
                    .Replace('Ź', 'Z').Replace('ź', 'z')
                    .Replace('Ż', 'Z').Replace('ż', 'z');

                var user = new ApplicationUser { UserName = getUserName, Email = getUserName };
                var result = await userManager.CreateAsync(user, getPasswordName);
                if (result.Succeeded)
                {
                    db.Workers.Add(NewWorker);
                    userManager.AddToRole(user.Id, "Worker");
                }             
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Worker worker = db.Workers.Find(id);
                if (worker == null)
                {
                    return HttpNotFound();
                }
                return View(worker);
            }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdWorker,Name,Surname,PrivatePhone,WorkPhone,City,Street,ZIPCode,HouseNumber,Pesel,IdNumber")] Worker worker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(worker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(worker);
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Worker worker = db.Workers.Find(id);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Worker worker = db.Workers.Find(id);
            db.Workers.Remove(worker);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}
