﻿using System;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using complaint_site.Models;
using complaint_site.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Mvc;
using System.Web;

namespace complaint_site.Controllers
{
    [YourCustomAuthorize(Roles = "Admin, Worker, Customer")]
    public class ReportsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            if (User.IsInRole("Customer") == true)
            {
                var checkUserID = User.Identity.GetUserId();
                var data = db.Reports.Where(v => v.User.Id == checkUserID).ToList();
                return View(data);
            }
            else
            {
                return View(db.Reports.ToList());
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Report data = db.Reports.Include("Images").Where(l => l.IdReport == id).FirstOrDefault();
            
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdReport,Description,Type,Category,Data_of_issue,Status,ReplyReport,SinceWhen,OthershaveProblem,Devices,SpeedInternet,AllSites,SiteUrl,ProgramName,ReasonForComplaint,PlaceComplaint,DuplicateContract,Expectations")] Report report, HttpPostedFileBase postedFile)
        {
            var userManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            ApplicationUser currentUser = userManger.FindById(User.Identity.GetUserId());
            Customer customer = db.Customers.Where(c => c.User.Id == currentUser.Id).SingleOrDefault();
            string userEmail = User.Identity.GetUserName();
            report.Status = "Do rozpatrzenia";

            var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
            if (ModelState.IsValid)
            {
                if (postedFile != null)
                {
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                    {
                        bytes = br.ReadBytes(postedFile.ContentLength);
                    }
                    Image img = new Image
                    {
                        Name = Path.GetFileName(postedFile.FileName),
                        ContentType = postedFile.ContentType,
                        Data = bytes,
                        IdReport = report.IdReport
                    };
                    db.Images.Add(img);
                }

                report.Customer = customer;
                report.User = currentUser;
                db.Reports.Add(report);
                db.SaveChanges();
                ViewBag.Added = "Sukces";

                ConfirmSendReportEmail email = new ConfirmSendReportEmail();
                email.To = userEmail;
                email.From = "develogic@h339.mikr.dev";
                email.IdReport = report.IdReport;
                email.Category = report.Category;
                email.Type = report.Type;
                email.Data_of_issue = report.Data_of_issue;
                email.Send();

                return RedirectToAction("Index");
            }

            return View(report);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Report report = db.Reports.Find(id);
            report.Description = "";
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(report);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdReport,Description,Type,Category,Data_of_issue,Status,ReplyReport,SinceWhen,OthershaveProblem,Devices,SpeedInternet,AllSites,SiteUrl,ProgramName,ReasonForComplaint,PlaceComplaint,DuplicateContract,Expectations")] Report report)
        {
            var errors = ModelState
           .Where(x => x.Value.Errors.Count > 0)
           .Select(x => new { x.Key, x.Value.Errors })
           .ToArray();
            if (ModelState.IsValid)
            {
                db.Entry(report).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(report);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Report report = db.Reports.Find(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(report);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Report report = db.Reports.Find(id);
            db.Reports.Remove(report);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ReplyReport(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = db.Reports.Where(d => d.IdReport == id).FirstOrDefault();
            var model = new ReportViewModel
            {
                IdReport = data.IdReport,
                Description = data.Description,
                Status = data.Status,
                ReplyReport = data.ReplyReport,
                Type = data.Type,
                Category = data.Category,
                Customer = data.Customer
            };
           
            if (id == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReplyReport(ReportViewModel report, 
            string success, string needSomeInfo, string failed, string begin)
        {
            if (success != null)
            {
                report.Status = "Rozpatrzono";
            }
            else if (needSomeInfo != null)
            {
                report.Status = "Wymaga dodatkowych informacji";
            }
            else if (failed != null)
            {
                report.Status = "Odrzucono";
            }
            else if (begin != null)
            {
                report.Status = "W trakcie realizacji";
            }
            var userManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            ApplicationUser currentUser = userManger.FindById(User.Identity.GetUserId());
            Worker worker = db.Workers.Where(c => c.User.Id == currentUser.Id).SingleOrDefault();
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {

                var data = db.Reports.Where(x => x.IdReport == report.IdReport).SingleOrDefault();
                Customer customer = db.Customers.Where(h => 
                h.IdCustomer == data.Customer.IdCustomer).SingleOrDefault();
                string userEmail = customer.User.Email;
                data.DataChangeStatus = DateTime.Now;
                data.Status = report.Status;
                data.ReplyReport = report.ReplyReport;
                data.Workers = worker;
                db.SaveChanges();
                AnswerForReportEmail email = new AnswerForReportEmail();
                email.To = userEmail;
                email.From = "develogic@h339.mikr.dev";
                email.Status = report.Status;
                email.ReplyReport = report.ReplyReport;
                email.Send();
                return RedirectToAction("Index");
            }
            return View(report);
        }

        public ActionResult AddAnswer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var data = db.Reports.Where(d => d.IdReport == id).FirstOrDefault();
            var model = new ReportViewModel
            {
                IdReport = data.IdReport,
                Description = "",
                ReplyReport = data.ReplyReport,
            };
            if (id == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAnswer(ReportViewModel report)
        {
            report.Status = "Do rozpatrzenia";
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var data = db.Reports.Where(x => x.IdReport == report.IdReport).SingleOrDefault();
                Customer customer = db.Customers.Where(h => h.IdCustomer == data.Customer.IdCustomer).SingleOrDefault();
                string userEmail = customer.User.Email;
                data.Data_of_issue = DateTime.Now;
                data.Status = "Do rozpatrzenia";
                data.ReplyReport = "";
                data.Description = report.Description;
                db.SaveChanges();
                
                ConfirmSendReportEmail email = new ConfirmSendReportEmail();
                email.To = userEmail;
                email.From = "develogic@h339.mikr.dev";
                email.IdReport = data.IdReport;
                email.Category = data.Category;
                email.Type = data.Type;
                email.Data_of_issue = data.Data_of_issue;
                email.Send();
                return RedirectToAction("Index");
            }
            return View(report);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
