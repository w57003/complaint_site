﻿using complaint_site.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(complaint_site.Startup))]
namespace complaint_site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            AddUserAndRole();
        }
        public void AddUserAndRole()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "admin@isp.com";
                user.Email = "admin@isp.com";
                string password = "P@$$w0rd";

                var userMgr = userManager.Create(user, password);

                if (userMgr.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Admin");
                }

            }

            if (!roleManager.RoleExists("Worker"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Worker";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "Worker@isp.com";
                user.Email = "Worker@isp.com";
                string password = "P@$$w0rd";

                var userMgr = userManager.Create(user, password);

                if (userMgr.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Worker");
                }

            }

            if (!roleManager.RoleExists("Customer"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Customer";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "Customer@isp.com";
                user.Email = "Customer@isp.com";
                string password = "P@$$w0rd";

                var userMgr = userManager.Create(user, password);

                if (userMgr.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Customer");
                }
            }
        }
    }
}
