﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace complaint_site.Models
{
    public class Report
    {
        [Key]
        public int IdReport { get; set; }
        [Required]
        [Display(Name = "Opis")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Typ zgłoszenia")]
        public string Type { get; set; }
        [Required]
        [Display(Name = "Kategoria zgłoszenia")]
        public string Category { get; set; }
        [Display(Name = "Data wystawienia zgłoszenia")]
        [DataType(DataType.Date)]
        public DateTime Data_of_issue { get; set; }
        [Display(Name = "Status zgłoszenia")]
        public string Status { get; set; }
        [Display(Name = "Odpowiedź pracownika")]
        public string ReplyReport { get; set; }
        [Display(Name = "Od kiedy wystepuje")]
        [DataType(DataType.Date)]
        public DateTime? SinceWhen { get; set; }
        [Display(Name = "Data rozpatrzenia")]
        [DataType(DataType.Date)]
        public DateTime? DataChangeStatus { get; set; }
        [Display(Name = "Czy inni mają problem")]
        public string OthershaveProblem { get; set; }
        [Display(Name = "Czy na innych urządzeniach występuje problem")]
        public string Devices { get; set; }
        [Display(Name = "Prędkość internetu ")]
        public string SpeedInternet { get; set; }
        [Display(Name = "Czy na wszystkich stronach występuje problem")]
        public string AllSites { get; set; }
        [DataType(DataType.Url)]
        [Display(Name = "Adres URL strony")]
        public string SiteUrl { get; set; }
        [Display(Name = "Nazwa programu")]
        public string ProgramName { get; set; }
        [Display(Name = "Powód")]
        public string ReasonForComplaint { get; set; }
        [Display(Name = "Miejsce wystapienia nieprzyjemności")]
        public string PlaceComplaint { get; set; }
        [Display(Name = "Wysyłka duplikatu umowy")]
        public string DuplicateContract { get; set; }
        [Display(Name = "Twoje oczekiwania")]
        public string Expectations { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Worker Workers { get; set; }

        //Dla wielu obrazków:
        public virtual ICollection<Image> Images { get; set; }
        //public virtual Image Image{ get; set; }
    }
}