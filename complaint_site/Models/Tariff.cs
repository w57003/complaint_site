﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace complaint_site.Models
{
    public class Tariff
    {
        [Key]
        public int IdTariff { get; set; }
        [Required]
        [Display(Name = "Nazwa taryfy")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Parametry")]
        public string Parameters { get; set; }
        [Required]
        [Display(Name = "Cena miesięczna")]
        public int Price { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}