﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace complaint_site.Models
{
	public class Image
	{
        [Key]
        public int IdImage { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public Byte[] Data { get; set; }
        public int IdReport { get; set; }
        public virtual Report Report { get; set; }
    }
}