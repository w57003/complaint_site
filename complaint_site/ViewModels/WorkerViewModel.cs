﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace complaint_site.ViewModels
{
    public class WorkerViewModel
    {
        public int IdWorker { get; set; }
        [Required]
        [Display(Name = "Imię")]
        [StringLength(50, MinimumLength = 2)]
        [RegularExpression("^[a-zA-ZzżźćńółęąśŻŹĆĄŚĘŁÓŃ]*$", ErrorMessage = "Imię zawiera tylko znaki od 'a' do 'z'")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Nazwisko")]
        [StringLength(50, MinimumLength = 2)]
        [RegularExpression("^[-a-zA-ZzżźćńółęąśŻŹĆĄŚĘŁÓŃ]*$", ErrorMessage = "Nazwisko zawiera tylko zanki od 'a' do 'z' i '-'")]
        public string Surname { get; set; }
        [Required]
        [Display(Name = "Telefon prywatny")]
        [StringLength(12, MinimumLength = 12)]
        [RegularExpression("^[+]{1}[0-9]{11}?$", ErrorMessage = "Wpisz numer w formacie: +48XXXXXXXXX")]
        public string PrivatePhone { get; set; }
        [Required]
        [Display(Name = "Telefon służbowy")]
        [StringLength(12, MinimumLength = 12)]
        [RegularExpression("^[+]{1}[0-9]{11}?$", ErrorMessage = "Wpisz numer w formacie: +48XXXXXXXXX")]
        public string WorkPhone { get; set; }
        [Required]
        [Display(Name = "Miasto")]
        [StringLength(50, MinimumLength = 2)]
        [RegularExpression("^[-a-zA-ZzżźćńółęąśŻŹĆĄŚĘŁÓŃ]*$", ErrorMessage = "Nazwa miejscowości zawiera tylko litery i '-' ")]
        public string City { get; set; }
        [Required]
        [Display(Name = "Ulica")]
        [StringLength(50, MinimumLength = 2)]
        public string Street { get; set; }
        [Required]
        [Display(Name = "Numer budynku/mieszkania")]
        [StringLength(10)]
        public string HouseNumber { get; set; }
        [Required]
        [Display(Name = "Kod pocztowy")]
        [StringLength(6, MinimumLength = 6)]
        [RegularExpression(@"^\d{2}(-\d{3})?$", ErrorMessage = "Format kodu poczotwego to: 00-000")]
        public string ZIPCode { get; set; }
        [Required]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Pesel zawiera 11 cyfr")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Pesel zawiera tylko cyfry")]
        public string Pesel { get; set; }
        [Required]
        [Display(Name = "Numer dowodu osobistego")]
        [StringLength(9, MinimumLength = 9)]
        [RegularExpression(@"^[a-zA-Z]{3}[0-9]{6}?$", ErrorMessage = "Format numeru dowodu osobistego to: XYZ123456")]
        public string IdNumber { get; set; }
    }
}