﻿using complaint_site.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace complaint_site.ViewModels
{
    public class ReportViewModel
    {
        public int IdReport { get; set; }
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Typ zgłoszenia")]
        public string Type { get; set; }

        [Display(Name = "Kategoria zgłoszenia")]
        public string Category { get; set; }
        public string Status { get; set; }
        [Display(Name = "Data wystawienia zgłoszenia")]
        [DataType(DataType.Date)]
        public DateTime Data_of_issue { get; set; }
        [Required]
        [Display(Name = "Odpowiedź na zgłoszenie")]
        public string ReplyReport { get; set; }
        [Display(Name = "Od kiedy wystepuje")]
        [DataType(DataType.Date)]
        public DateTime? SinceWhen { get; set; }
        [Display(Name = "Data rozpatrzenia")]
        [DataType(DataType.Date)]
        public DateTime? DataChangeStatus { get; set; }
        [Display(Name = "Czy inni mają problem")]
        public string OthershaveProblem { get; set; }
        [Display(Name = "Czy na innych urządzeniach występuje problem")]
        public string Devices { get; set; }
        [Display(Name = "Prędkość internetu ")]
        public string SpeedInternet { get; set; }
        [Display(Name = "Czy na wszystkich stronach występuje problem")]
        public string AllSites { get; set; }
        [DataType(DataType.Url)]
        [Display(Name = "Adres URL strony")]
        public string SiteUrl { get; set; }
        [Display(Name = "Nazwa programu")]
        public string ProgramName { get; set; }
        [Display(Name = "Powód reklamacji ")]
        public string ReasonForComplaint { get; set; }
        [Display(Name = "Miejsce wystapienia nieprzyjemności")]
        public string PlaceComplaint { get; set; }
        [Display(Name = "Wysyłka duplikatu umowy")]
        public string DuplicateContract { get; set; }
        [Display(Name = "Oczekiwania")]
        public string Expectations { get; set; }
        public ApplicationUser User { get; set; }
        public byte[] Data { get; set; }
        public int MyProperty { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Worker Workers { get; set; }

    }
}