﻿namespace complaint_site.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        IdCustomer = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Surname = c.String(nullable: false, maxLength: 50),
                        City = c.String(nullable: false, maxLength: 50),
                        Street = c.String(nullable: false, maxLength: 50),
                        HouseNumber = c.String(nullable: false, maxLength: 10),
                        ZIPCode = c.String(nullable: false, maxLength: 6),
                        CityCorespondence = c.String(maxLength: 50),
                        StreetCorespondence = c.String(maxLength: 50),
                        HouseNumberCorespondence = c.String(maxLength: 10),
                        ZIPCodeCorespondence = c.String(maxLength: 6),
                        Pesel = c.String(maxLength: 11),
                        IdNumber = c.String(nullable: false, maxLength: 9),
                        Phone = c.String(nullable: false, maxLength: 12),
                        IdTariff = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdCustomer)
                .ForeignKey("dbo.Tariffs", t => t.IdTariff, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.IdTariff)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        IdReport = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        Category = c.String(nullable: false),
                        Data_of_issue = c.DateTime(nullable: false),
                        Status = c.String(),
                        ReplyReport = c.String(),
                        SinceWhen = c.DateTime(),
                        DataChangeStatus = c.DateTime(),
                        OthershaveProblem = c.String(),
                        Devices = c.String(),
                        SpeedInternet = c.String(),
                        AllSites = c.String(),
                        SiteUrl = c.String(),
                        ProgramName = c.String(),
                        ReasonForComplaint = c.String(),
                        PlaceComplaint = c.String(),
                        DuplicateContract = c.String(),
                        Expectations = c.String(),
                        Customer_IdCustomer = c.Int(),
                        User_Id = c.String(maxLength: 128),
                        Workers_IdWorker = c.Int(),
                    })
                .PrimaryKey(t => t.IdReport)
                .ForeignKey("dbo.Customers", t => t.Customer_IdCustomer)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .ForeignKey("dbo.Workers", t => t.Workers_IdWorker)
                .Index(t => t.Customer_IdCustomer)
                .Index(t => t.User_Id)
                .Index(t => t.Workers_IdWorker);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        IdImage = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ContentType = c.String(),
                        Data = c.Binary(),
                        IdReport = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdImage)
                .ForeignKey("dbo.Reports", t => t.IdReport, cascadeDelete: true)
                .Index(t => t.IdReport);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Workers",
                c => new
                    {
                        IdWorker = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Surname = c.String(nullable: false, maxLength: 50),
                        PrivatePhone = c.String(nullable: false, maxLength: 12),
                        WorkPhone = c.String(nullable: false, maxLength: 12),
                        City = c.String(nullable: false, maxLength: 50),
                        Street = c.String(nullable: false, maxLength: 50),
                        HouseNumber = c.String(),
                        Pesel = c.String(maxLength: 11),
                        IdNumber = c.String(nullable: false, maxLength: 10),
                        ZIPCode = c.String(maxLength: 6),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdWorker)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Tariffs",
                c => new
                    {
                        IdTariff = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Parameters = c.String(nullable: false),
                        Price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdTariff);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Customers", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Customers", "IdTariff", "dbo.Tariffs");
            DropForeignKey("dbo.Workers", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Reports", "Workers_IdWorker", "dbo.Workers");
            DropForeignKey("dbo.Reports", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Images", "IdReport", "dbo.Reports");
            DropForeignKey("dbo.Reports", "Customer_IdCustomer", "dbo.Customers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Workers", new[] { "User_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Images", new[] { "IdReport" });
            DropIndex("dbo.Reports", new[] { "Workers_IdWorker" });
            DropIndex("dbo.Reports", new[] { "User_Id" });
            DropIndex("dbo.Reports", new[] { "Customer_IdCustomer" });
            DropIndex("dbo.Customers", new[] { "User_Id" });
            DropIndex("dbo.Customers", new[] { "IdTariff" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Tariffs");
            DropTable("dbo.Workers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Images");
            DropTable("dbo.Reports");
            DropTable("dbo.Customers");
        }
    }
}
