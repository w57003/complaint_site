﻿var type = document.querySelector("#Type");

var category = document.querySelector("#Category");
var categoryOrderForm = document.querySelector("#categoryOrder");

var descriptionForm = document.querySelector("#description");

var duplicateContractForm = document.querySelector("#duplicateContract");
var duplicateContract = document.querySelector("#DuplicateContract");

var sinceWhen = document.querySelector("#SinceWhen");
var sinceWhenForm = document.querySelector("#sinceWhen");
var sinceWhenLabel = document.querySelector("#sinceWhenLabel");
var forgetSinceWhen = document.querySelector("#forgetSinceWhen");
var forgetSinceWhenCheckbox = document.querySelector("#forgetSinceWhenCheckbox");
var techSinceWhen = document.querySelector("#techSinceWhen");
var techSinceWhenCheckbox = document.querySelector("#techSinceWhenCheckbox");

var dataOfIssue = document.querySelector("#Data_of_issue");

//https://stackoverflow.com/questions/1531093/how-do-i-get-the-current-date-in-javascript
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
var time = String(today.getHours()).padStart(2, '0')
    + ":" + String(today.getMinutes()).padStart(2, '0') + ":"
    + String(today.getSeconds()).padStart(2, '0');
today = yyyy + '-' + mm + '-' + dd + " " + time;
var onlyDate = yyyy + '-' + mm + '-' + dd;

sinceWhen.max = today;
dataOfIssue.value = today;

var othershaveProblem = document.querySelector("#OthershaveProblem");
var othershaveProblemForm = document.querySelector("#othershaveProblem");

var devices = document.querySelector("#Devices");
var devicesForm = document.querySelector("#devices");

var speedInternet = document.querySelector("#SpeedInternet");
var speedInternetForm = document.querySelector("#speedInternet");
var speedInternetCheckbox = document.querySelector("#speedInternetCheckbox");

var allSites = document.querySelector("#AllSites");
var allSitesForm = document.querySelector("#allSites");

var siteUrl = document.querySelector("#SiteUrl");
var siteUrlForm = document.querySelector("#siteUrl");
var siteUrlCheckbox = document.querySelector("#siteUrlCheckbox");

var programName = document.querySelector("#ProgramName");
var programNameForm = document.querySelector("#programName");

var reasonForComplaint = document.querySelector("#ReasonForComplaint");
var reasonForComplaintForm = document.querySelector("#reasonForComplaint");

var placeComplaint = document.querySelector("#PlaceComplaint");
var placeComplaintForm = document.querySelector("#placeComplaint");

var duplicateContract = document.querySelector("#DuplicateContract");
var duplicateContractForm = document.querySelector("#duplicateContract");

var expectations = document.querySelector("#Expectations");
var expectationsForm = document.querySelector("#expectations");

var typeComplaintForm = document.querySelector("#typeComplaint")

var image = document.querySelector("#Images");
var imageForm = document.querySelector("#images");

var submitForm = document.querySelector("#submitForm");

var categoryComplaint = {
    "Reklamacja usługi": "Reklamacja usługi",
    "Reklamacja opłaty za naprawę": "Reklamacja opłaty za naprawę",
    "Inne": "Inne"
}

var categoryorderArray = {
    "Przekierownie portów": "Przekierownie portów",
    "Publiczny adres IP - dynamiczny": "Publiczny adres IP - dynamiczny",
    "Publiczny adres IP - statyczny": "Publiczny adres IP - statyczny",
    "Duplikat umowy": 'Duplikat umowy',
    "Usunięcie danych z bazy": "Usunięcie danych z bazy"
};

var categoryComplaintArray = {
    "Skarga na pracownika": "Skarga na pracownika",
    "Skarga na firmę": "Skarga na firmę",
    "Skarga na działanie usługi": "Skarga na działanie usługi"
}

var categoryTechProblemArray = {
    "Brak internetu": "Brak internetu",
    "Powolny internet": "Powolny internet",
    "Nie otwiera stron": "Nie otwiera stron",
    "Inne": "Inne"
}

var reasonForComplaintWorkerArray = {
    "Niewłaściwe zachowanie pracownika": "Niewłaściwe zachowanie pracownika",
    "Brak odpowiedzi od pracownika": "Brak odpowiedzi od pracownika",
    "Brak kompetencji": "Brak kompetencji",
    "Inne": "Inne",
}

var reasonForComplaintCompanyArray = {
    "Długi czas na odpowiedź": "Długi czas na odpowiedź",
    "Działania firmy": "Działania firmy",
    "Niekompetentna obsługa": "Niekompetentna obsługa",
    "Inne": "Inne"
}

var reasonForComplaintServiceArray = {
    "Notoryczne spowolnienia usługi": "Notoryczne spowolnienia usługi",
    "Częsty brak usługi": "Częsty brak usługi",
    "Długi czas oczekiwania na naprawę": "Długi czas oczekiwania na naprawę",
    "Zawieszające się strony": "Zawieszające się strony",
    "Inne": "Inne"
}

function HideAll() {
    categoryOrderForm.classList.add("d-none");
    allSitesForm.classList.add("d-none");
    descriptionForm.classList.add("d-none");
    devicesForm.classList.add("d-none");
    duplicateContractForm.classList.add("d-none");
    expectationsForm.classList.add("d-none");
    othershaveProblemForm.classList.add("d-none");
    placeComplaintForm.classList.add("d-none");
    programNameForm.classList.add("d-none");
    reasonForComplaintForm.classList.add("d-none");
    sinceWhenForm.classList.add("d-none");
    siteUrlForm.classList.add("d-none");
    submitForm.classList.add("d-none");
    imageForm.classList.add("d-none");
}
function hideTech() {
    programNameForm.classList.add("d-none");
    devicesForm.classList.add("d-none");
    siteUrlForm.classList.add("d-none");
    speedInternetForm.classList.add("d-none");
    hideDescriptionSubmit();
    imageForm.classList.add("d-none");
    othershaveProblemForm.classList.add("d-none");
    sinceWhenForm.classList.add("d-none");
    allSitesForm.classList.add("d-none");
};
function complaintOptions() {
    category.options.length = 1;
    categoryOrderForm.classList.remove("d-none");
    for (index in categoryComplaint) {
        category.options[category.options.length] = new Option(categoryComplaint[index], index);
    }
};

function categoryOrderOptions() {
    category.options.length = 1;
    categoryOrderForm.classList.remove("d-none");
    for (index in categoryorderArray) {
        category.options[category.options.length] = new Option(categoryorderArray[index], index);
    }
}

function categoryComplaintOptions() {
    category.options.length = 1;
    categoryOrderForm.classList.remove("d-none");
    for (index in categoryComplaintArray) {
        category.options[category.options.length] = new Option(categoryComplaintArray[index], index);
    }
}

function categoryTechProblemOptions() {
    category.options.length = 1;
    categoryOrderForm.classList.remove("d-none");
    for (index in categoryTechProblemArray) {
        category.options[category.options.length] = new Option(categoryTechProblemArray[index], index);
    }
}

function reasonForComplaintWorkerOptions() {
    reasonForComplaint.options.length = 1;
    reasonForComplaintForm.classList.remove("d-none");
    for (index in reasonForComplaintWorkerArray) {
        reasonForComplaint.options[reasonForComplaint.options.length] = new Option(reasonForComplaintWorkerArray[index], index);
    }
}

function reasonForComplaintCompanyOptions() {
    reasonForComplaint.options.length = 1;
    reasonForComplaintForm.classList.remove("d-none");
    for (index in reasonForComplaintCompanyArray) {
        reasonForComplaint.options[reasonForComplaint.options.length] = new Option(reasonForComplaintCompanyArray[index], index);
    }
}

function reasonForComplaintServiceOptions() {
    reasonForComplaint.options.length = 1;
    reasonForComplaintForm.classList.remove("d-none");
    for (index in reasonForComplaintServiceArray) {
        reasonForComplaint.options[reasonForComplaint.options.length] = new Option(reasonForComplaintServiceArray[index], index);
    }
}


function showDescriptionSubmit() {
    descriptionForm.classList.remove("d-none");
    submitForm.classList.remove("d-none");
}
function hideDescriptionSubmit() {
    descriptionForm.classList.add("d-none");
    submitForm.classList.add("d-none");
}

function techSinceWhenCheckboxFunction() {
    sinceWhenForm.classList.remove("d-none");
    sinceWhenLabel.innerHTML = "Data wystąpienia problemu";
    techSinceWhenCheckbox.classList.remove("d-none");
    techSinceWhen.addEventListener("change", function () {
        if (techSinceWhen.checked == true) {
            sinceWhen.value = today;
        }
    })
}

function afterCheckboxtechproblem() {
    othershaveProblemForm.classList.remove("d-none");
    othershaveProblem.addEventListener("change", function () {
        switch (othershaveProblem.value) {
            case "":
                devicesForm.classList.add("d-none");
                hideDescriptionSubmit();
                imageForm.classList.add("d-none");
                break;
            default:
                devicesForm.classList.remove("d-none");
                devices.addEventListener("change", function () {
                    switch (devices.value) {
                        case "":
                            hideDescriptionSubmit();
                            imageForm.classList.add("d-none");
                            break;
                        default:
                            showDescriptionSubmit();
                            imageForm.classList.remove("d-none");
                            break;
                    }

                });
                break;
        }
    });
}

function speedInternetCheckboxFunction() {
    speedInternetCheckbox.addEventListener("change", function () {
        if (speedInternetCheckbox.checked == true) {
            speedInternet.disabled = true;
        }
        else {
            speedInternet.disabled = false;
        }
    });

}
function siteUrlCheckboxFunction() {
    siteUrlCheckbox.addEventListener("change", function () {
        if (siteUrlCheckbox.checked == true) {
            siteUrl.disabled = true;
            siteUrl.required = false;
        }
        else {
            siteUrl.disabled = false;
            siteUrl.required = true;
        }
    });
}

function checkBoxToday() {
    allSitesForm.classList.remove("d-none");
    allSites.addEventListener("change", function () {
        switch (allSites.value) {
            case "TAK":
                programNameForm.classList.add("d-none");
                siteUrlForm.classList.add("d-none");
                showDescriptionSubmit();
                imageForm.classList.remove("d-none");
                break;
            case "NIE":
                programNameForm.classList.add("d-none");
                siteUrlForm.classList.remove("d-none");
                siteUrlCheckboxFunction();
                showDescriptionSubmit();
                imageForm.classList.remove("d-none");
                break;
            case "Problem dotyczy programu":
                siteUrlForm.classList.add("d-none");
                programNameForm.classList.remove("d-none");
                showDescriptionSubmit();
                imageForm.classList.remove("d-none");
                break;
            default:
                programNameForm.classList.add("d-none");
                devicesForm.classList.add("d-none");
                siteUrlForm.classList.add("d-none");
                hideDescriptionSubmit();
                imageForm.classList.add("d-none");
                programName.classList.add("d-none");
                break;
        }
    });
}

type.addEventListener("change", function () {
    switch (type.value) {
        case "Zlecenie":
            HideAll();
            categoryOrderOptions();
            category.addEventListener("change", function () {
                switch (category.value) {
                    case "Duplikat umowy":
                        duplicateContractForm.classList.remove("d-none");
                        duplicateContract.addEventListener("change", function () {
                            switch (duplicateContract.value) {
                                case "E-Mail":
                                    showDescriptionSubmit();
                                    break;
                                case "Adres korespondencyjny":
                                    showDescriptionSubmit();
                                    break;
                                default:
                                    hideDescriptionSubmit();
                                    break;
                            }
                        });
                        break;
                    case "":
                        duplicateContractForm.classList.add("d-none");
                        hideDescriptionSubmit()
                        break;
                    default:
                        duplicateContractForm.classList.add("d-none");
                        showDescriptionSubmit()
                        break;
                }
            });
            break;
        case "Reklamacja":
            HideAll();
            complaintOptions();
            category.addEventListener("change", function () {
                switch (category.value) {
                    case "":
                        hideDescriptionSubmit()
                        imageForm.classList.add("d-none");
                        expectationsForm.classList.add("d-none");
                        break;
                    default:
                        showDescriptionSubmit();
                        imageForm.classList.remove("d-none");
                        expectationsForm.classList.remove("d-none");
                };
            });
            break;
        case "Skarga":
            HideAll();
            categoryComplaintOptions();
            category.addEventListener("change", function () {
                switch (category.value) {
                    case "Skarga na pracownika":
                        reasonForComplaintWorkerOptions();
                        reasonForComplaint.addEventListener("change", function () {
                            switch (reasonForComplaint.value) {
                                case "":
                                    hideDescriptionSubmit();
                                    placeComplaintForm.classList.add("d-none");
                                    sinceWhenForm.classList.add("d-none");

                                    break;
                                default:
                                    placeComplaintForm.classList.remove("d-none");
                                    placeComplaint.addEventListener("change", function () {
                                        switch (placeComplaint.value) {
                                            case "":
                                                sinceWhenForm.classList.add("d-none");
                                                sinceWhenLabel.innerHTML = "";
                                                hideDescriptionSubmit();
                                                expectationsForm.classList.add("d-none");
                                                break;
                                            default:
                                                sinceWhenForm.classList.remove("d-none");
                                                forgetSinceWhenCheckbox.classList.remove("d-none");
                                                sinceWhenLabel.innerHTML = "Data nieprzyjemności";
                                                forgetSinceWhen.addEventListener("change", function () {
                                                    if (forgetSinceWhen.checked == true) {
                                                        sinceWhen.disabled = true;
                                                    }
                                                    else {
                                                        sinceWhen.disabled = false;
                                                    }

                                                });
                                                expectationsForm.classList.remove("d-none");
                                                showDescriptionSubmit();
                                                break;
                                        }
                                    });
                                    expectationsForm.classList.add("d-none");
                                    break;

                            }
                        });
                        break;
                    case "Skarga na firmę":
                        reasonForComplaintCompanyOptions();
                        placeComplaintForm.classList.add("d-none");
                        reasonForComplaint.addEventListener("change", function () {
                            switch (reasonForComplaint.value) {
                                case "":
                                    expectationsForm.classList.add("d-none");
                                    hideDescriptionSubmit();
                                    break;
                                default:
                                    expectationsForm.classList.remove("d-none");
                                    showDescriptionSubmit();
                                    break;

                            }
                        });
                        break;
                    case "Skarga na działanie usługi":
                        reasonForComplaintServiceOptions();
                        reasonForComplaintForm.addEventListener("change", function () {
                            switch (reasonForComplaint.value) {
                                case "":
                                    expectationsForm.classList.add("d-none");
                                    hideDescriptionSubmit();
                                    break;
                                default:
                                    expectationsForm.classList.remove("d-none");
                                    showDescriptionSubmit();
                                    break;

                            } 
                        });
                        break;
                    default:
                        expectationsForm.classList.add("d-none");
                        reasonForComplaintForm.classList.add("d-none");
                        placeComplaintForm.classList.add("d-none");
                        sinceWhenForm.classList.add("d-none");
                        hideDescriptionSubmit();
                        break;
                }
            });
            break;
        case "Problem techniczny":
            HideAll();
            categoryTechProblemOptions();
            category.addEventListener("change", function () {
                switch (category.value) {
                    case "Brak internetu":
                        hideTech();
                        techSinceWhenCheckboxFunction();
                        sinceWhen.addEventListener("change", function () {
                            afterCheckboxtechproblem();
                            allSitesForm.classList.add("d-none");
                        });
                        techSinceWhenCheckbox.addEventListener("change", function () {
                            afterCheckboxtechproblem();
                        });
                        break;
                    case "Powolny internet":
                        hideTech();
                        techSinceWhenCheckboxFunction();
                        sinceWhen.addEventListener("change", function () {
                            devicesForm.classList.remove("d-none");
                            devices.addEventListener("change", function () {
                                switch (devices.value) {
                                    case "":
                                        speedInternetForm.classList.add("d-none");
                                        imageForm.classList.add("d-none");
                                        hideDescriptionSubmit();
                                        break;
                                    default:
                                        speedInternetForm.classList.remove("d-none");
                                        speedInternetCheckboxFunction();
                                        imageForm.classList.remove("d-none");
                                        showDescriptionSubmit();
                                        break;
                                }
                            });
                        });

                        break;
                    case "Nie otwiera stron":
                        hideTech();
                        techSinceWhenCheckboxFunction();
                        techSinceWhen.addEventListener("change", function () {
                            checkBoxToday();
                        });
                        sinceWhen.addEventListener("change", function () {
                            checkBoxToday();
                        });
                        break;
                    case "Inne":
                        hideTech();
                        showDescriptionSubmit();
                        imageForm.classList.remove("d-none");
                        break;
                    default:
                        hideTech();
                        break;
                }
            });
            break;
        default:
            HideAll();
            break;
    }
});

$(document).ready(function () {
    $('textarea').on("focus input", function () {
        $(this).css('height', 'auto').css('height', this.scrollHeight + (this.offsetHeight - this.clientHeight));
    });
});